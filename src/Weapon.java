public class Weapon {
    private String name;
    private int damage;

    public Weapon(String name, int damage){
        this.name = name;
        this.damage = damage;
    }
    public void Gun(){
        System.out.println(name+" "+damage);
        
    }
}
